#!/bin/sh

# Check for GitLab ENTRYPOINT double execution bug (https://gitlab.com/gitlab-org/gitlab-runner/issues/1380)

# Setup kubeconfig
if [ ! -z "${KUBECONFIG_BASE64}" ] && [ ! -f "~/.kube/config" ]; then
    mkdir -p ~/.kube
    echo "${KUBECONFIG_BASE64}" | base64 -d > ~/.kube/config
    chmod 600 ~/.kube/config
fi

exec /bin/bash